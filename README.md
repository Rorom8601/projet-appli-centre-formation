# Projet "Training center"
### Membres du groupe :
- Safik K.
- Romain S.
- Mohammad S.
- Camille T.
>## Conception
>> ### Diagramme de Use Case

Pour notre projet d'application de gestion d'un centre de formation, nous avons défini 3 groupes d'utilisateurs, pouvant chacun utiliser des fonctionnalités différentes de l'application.
<ul>
<li> Le personnel administratif du centre de formation (<b>Admin Staff</b>)</li>
<ul>

<li><i>Peut gérer les CRUD des apprenants, des formateurs et d'autres admin staff</i></li>
<li><i>Peut gérer les CRUD des formations et des sessions de formations</i></li>
<li><i>lister les apprenants d'une session</i></li>
<li><i>obtenir l'adresse de n'importe quel étudiant ou memebre de l'organisme de formation</i></li>
</ul>  

<li>Les formateurs (Teacher)
<ul>
<li><i>Peuvent valider les compétences d'un apprenant</i></li>
</ul>
<li>Les apprenants (Students)</li>  
<ul>
<li><i>Peuvent consulter leur propre progression (skills validées/skills à valider)</i></li>
<li><i>Peuvent consulter si une compétence particulière a été validée</i></li>
</ul>
</ul>

Nous avions pensé à une fonctionnalité qui permettrait à l'apprenant de poster un projet et au formateur de le corriger mais nous ne voyions pas comment la mettre en oeuvre et craignions de manquer de temps pour le faire, nous avons abandonné cette idée.

Nous avions imaginé une fonctionnalité optionnelle qui aurait permis aux différents acteurs de consulter la progression de l'ensemble des apprenants de la session, mais avons manqué de temps pour la développer.

>>### Diagramme de classe

Le diagramme de classe que nous avons créé lors de la conception de notre appli se base sur les tables dans la base de données, incluant des tables de jointures et ne tenant pas compte de toutes les classes java.
Nous avons travaillé comme s'il s'agissait d'un MCD pour concevoir une BDD qui permette d'implémenter nos fonctionnalités et qui soit cohérente. Vous trouverez d'ailleurs à la racine du projet un MCD de notre BDD généré grâce à mySQL Workbench au format PDF.

Pour répondre aux besoins de notre projet, côté BDD, nous avons de créer 9 tables :

- 3 tables correspondant aux différents acteurs/utilisateurs de l'appli (Formateurs, apprenants et personnel administratif)
- 4 tables pour les entités Formation, Adresse, Session et Compétences.
- 2 tables de jointures, student_skill et formation_skill

Nous avons relié ces tables entre elles par des relations 1-1, 1-* et *-*.
- Les formateurs/apprenants/admin ont chacun une adresse (1)
- Une même addresse peut appartenir à plusieurs formateurs/apprenants/admin (*)

- Une session ne porte que sur une formation (1)
- Une formation peut être dispensée dans plusieurs sessions (*)

- Une formation permet de développer plusieurs compétences (*)
- Une même compétence peut être développée dans plusieurs formations (*)

- Un apprenant/formateur fait partie d'une seule session (1)
- Une session peut recenser plusieurs apprenants/formateurs (*)

- Un apprenant peut développer plusieurs compétences(*)
- Une même compétence peut être développée par plusieurs apprenants(*)

>## Programmation
>>### SQL
Nous avons créé le  script sql permettant de générer la structure de la bdd ainsi qu'un jeux de données pour tester les contraintes & relations de tables .
Nous avons sauvegardé un fichier sql contenant quelques unes de nos requêtes "test" pour être sûr que la structure de note BDD puisse nous permettre de naviguer d'une table à l'autre et accomplir les fonctionnalités que nous avions définies.

>>### JDBC
Nous avons un package entity qui contient une classe pour chaque entité et un package repository qui contient la couche d'accès à la BDD (une classe Repository par entité). Nous avons également une classe DbUtil dans laquelle nous avons externalisé la méthode de connexion à la BDD, ce qui nous a permis d'uniformiser un peu le code (au départ, nous n'avions pas tous le même nom de base de donnée en local).

>>>#### Package entiy
Les classes entity contiennent les propriétés de chaque entité de la BDD (les différents champs des tables) avec les types Java correspondants, les constructeurs et les getter/setter car nous avons réglé l'accessibilité de nos propiétés à "private". Les clés étrangères sont représentées dans les classes entity par des variables de Type objet faisant référence à l'entité avec laquelle elles sont en relation dans la BDD.
Nous avons créé des constructeurs plus ou moins surchargés en fonction de nos besoins (pas d'id pour un insert par exemple car auto généré par le SGDBR).

>>>#### Package repository
Les classes repository contiennent les méthodes java permettant d'intéragir avec la BDD.

Chaque entité du MCD contient les fonctionnalités de base (findAll(), findById(), create(), update(), delete()).

>>>>##### Zoom sur les méthodes

- AddressRepository

  Nous avons implementé dans cette classe les méthodes permettant de récupérer l'adresse de chaque protagoniste.
 Cette fonctionnalité est utile à plusieurs titres, elle nous permet en plus de sa fonctionnalité de base d'appeler ces fonctions à la création d'entités contenant des adresses (student, teacher, admin_staff), et donc de s'économiser d'écrire un nouveau PreparedStatement et un nouveau ResultSet. Ce qui permet d'appliquer la règle de bonne pratique nommé DRY (Don't repeat yourself).

- StudentRepository

  La fonction checkProgressionForCertification() permet, en entrant l'id de l'étudiant, de connaître sa progression (nbSkills validées/ nbSkillsAValider) exprimée en pourcentage. Le retour de cette fonction est de type String. 
  La méthode checkIfStudentHasSpecificSkill() permet de vérifier la validation (ou non) d'une compétence donnée.
  La fonction attributeSkillToAStudent(), qui est destinée à être à la main des formateurs, permet d'attribuer une compétence à un apprenant.

- SessionRepository

La fonction getStudentListByIdSession() permet d'obtenir la liste des apprenants pour une session.

>>>>##### Contrôles "métiers"

Nous avons effectué quelques contrôles "métiers" sur les fonctions d'insertion, notamment sur la cohérence entre la date de début de formation et la date de fin, cette dernière devant être ultérieure à la première pour valider l'enregistrement dans la BDD.
Les autres contrôles sur le type ne sont pas extrêmement pertinents dans ce cas puisque l'application refuserait de compiler si était entré un type ne correspondant pas.

>>>> ##### Test de l'appli
Il y a dans la classe App, dans le "main", une série de tests groupés par repository qui vous permettront de tester facilement la plupart de nos fonctions, certaines fonctions d'insertions et de suppressions sont commentées car elle n'ont pas vocation à être lancées à chaque test. Il vous suffira de les décommenter pour les tester.



Vous trouverez le fichier training_center_creation_and_dataSet.sql qui vous permettra de générer la BDD et de la peupler.

>## Organisation
>>### Git
Nous avons travaillé simultanément sur le projet en utilisant gitlab. Malgré le fait que nous nous étions assignés des fichiers différents sur lesquels travailler, nous nous sommes retrouvés à devoir gérer des conflits git assez régulièrement. Face à ce problème, nous avons pu mettre le doigt sur l'importance de bien configurer notre fichier gitignore avant d'ajouter nos fichiers au dépôt.

>>### Structure de la BDD
Chaque membre du groupe a travaillé sur sa propre version d'un script de création de la base de donnée. Même si ce n'était pas la façon la plus rapide de procéder, cela permettait à chacun de "s'entrainer" à structurer une BDD.

>>### Convention
Nous avons essayé de respecter au maximum une convention commune en s'inspirant des bonnes pratiques connues :
- utiliser l'anglais comme langue de référence pour le projet
- nommage en lowercase pour mysql, PascalCase, camelCase en java...

