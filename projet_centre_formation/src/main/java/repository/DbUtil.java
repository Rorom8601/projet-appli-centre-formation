package repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbUtil {

  public static Connection connectToDb() throws SQLException {
    //à modifier selon le nom de la BDD en local
    return DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/training_center");

  }
  
}
