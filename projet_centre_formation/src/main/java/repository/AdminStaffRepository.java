package repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import entity.Address;
import entity.AdminStaff;

public class AdminStaffRepository {
  public List<AdminStaff> findAll() {
    List<AdminStaff> list = new ArrayList<>();

    try (Connection connection = DbUtil.connectToDb()) {

      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM admin_staff");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        int idAdmin=rs.getInt("id");
        AdminStaff adminStaff = new AdminStaff(idAdmin, rs.getString("name"),
            rs.getString("first_name"), rs.getDate("birth_date").toLocalDate(),rs.getString("mail_address"));
        //Récupération de l adresse
        AddressRepository ad=new AddressRepository();
        Address address=new Address();
        address=ad.findAddressByIdAdminStaff(idAdmin);
        adminStaff.setAddress(address);
        list.add(adminStaff);
      }
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return list;
  }

  public AdminStaff findById(int id) {
    try (Connection connection = DbUtil.connectToDb()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM admin_staff WHERE id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        AdminStaff adminStaff = new AdminStaff(rs.getInt("id"), rs.getString("name"),
        rs.getString("first_name"), rs.getDate("birth_date").toLocalDate(),rs.getString("mail_address"));

        //GET ADDRESS
        AddressRepository ad =new AddressRepository();
        Address address=new Address();
        address=ad.findAddressByIdAdminStaff(id);
        adminStaff.setAddress(address);
        return adminStaff;
      }
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return null;
  }

  public void deleteById(int id) {
    try (Connection connection = DbUtil.connectToDb()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM admin_staff WHERE id=?");

      stmt.setInt(1, id);
      stmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();

    }
  }

  public void save(AdminStaff adminStaff) {
    try (Connection connection = DbUtil.connectToDb()) {
      PreparedStatement stmt = connection.prepareStatement(
          "INSERT INTO admin_staff (name, first_name, birth_date, mail_address) VALUES (?,?,?,?)",
          PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1,adminStaff.getName());
      stmt.setString(2,adminStaff.getFirstName());
      stmt.setDate(3, Date.valueOf(adminStaff.getBirthDate()));
      stmt.setString(4,adminStaff.getMailAddress());
      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        adminStaff.setId(rs.getInt(1));
      }

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

public void update(AdminStaff adminStaff) {
    try (Connection connection = DbUtil.connectToDb()) {
      PreparedStatement stmt = connection.prepareStatement(
          "UPDATE admin_staff SET name=?,first_name=?, birth_date=?, mail_address = ? WHERE id=?");

      stmt.setString(1, adminStaff.getName());
      stmt.setString(2, adminStaff.getFirstName());
      stmt.setDate(3, Date.valueOf(adminStaff.getBirthDate()));
      stmt.setString(4, adminStaff.getMailAddress());
      stmt.setInt(6, adminStaff.getId());
      stmt.executeUpdate();
      

    } catch (SQLException e) {
      e.printStackTrace();

    }
  }

}

