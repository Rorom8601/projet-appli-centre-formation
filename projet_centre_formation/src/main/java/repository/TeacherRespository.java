package repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import entity.Teacher;
import entity.Address;
import entity.Session;

public class TeacherRespository {
  public List<Teacher> findAll() {
    List<Teacher> list = new ArrayList<>();
    try {
      Connection connection = DbUtil.connectToDb();

      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM teacher");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        int idTeacher = rs.getInt("id");
        Teacher tea = new Teacher(idTeacher, rs.getString("name"), rs.getString("first_name"),
            rs.getDate("birth_date").toLocalDate(), rs.getString("mail_address"));

        // Get the session
        PreparedStatement stmtSession = connection.prepareStatement(
            "SELECT * FROM session INNER JOIN teacher ON teacher.id_session=session.id WHERE teacher.id= ?");
        stmtSession.setInt(1, idTeacher);
        ResultSet rsSession = stmtSession.executeQuery();
        while (rsSession.next()) {
          Session session =
              new Session(rs.getInt("id"), rsSession.getDate("starting_date").toLocalDate(),
                  rsSession.getDate("ending_date").toLocalDate());
          tea.setSession(session);
        }
        list.add(tea);
      }
      connection.close();

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return list;
  }

  public void createTeacher(Teacher teacher) {
    try {
      Connection connection = DbUtil.connectToDb();

      PreparedStatement stmt = connection.prepareStatement(
          "INSERT INTO teacher( name, first_name, birth_date, mail_address) VALUES (?,?,?,?)",
          PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1, teacher.getName());
      stmt.setString(2, teacher.getFirstName());
      stmt.setDate(3, Date.valueOf(teacher.getBirthDate()));
      stmt.setString(4, teacher.getEmail());

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        teacher.setId(rs.getInt(1));
      }

      connection.close();

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

  public Teacher findById(int id) {
    try {
      Connection connection = DbUtil.connectToDb();

      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM teacher WHERE id =?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        Teacher teacher =
            new Teacher(rs.getInt("id"), rs.getString("name"), rs.getString("first_name"),
                rs.getDate("birth_date").toLocalDate(), rs.getString("mail_address"));

        // Get address
        AddressRepository ad = new AddressRepository();
        Address address = new Address();
        address = ad.findAddressByIdTeacher(id);
        teacher.setAddress(address);
        // Get session
        PreparedStatement stmtSession = connection.prepareStatement(
            "SELECT * from session INNER JOIN teacher ON teacher.id_session=session.id WHERE teacher.id= ?");
        stmtSession.setInt(1, id);
        ResultSet rsSession = stmtSession.executeQuery();
        while (rsSession.next()) {
          Session session =
              new Session(rs.getInt("id"), rsSession.getDate("starting_date").toLocalDate(),
                  rsSession.getDate("ending_date").toLocalDate());
          teacher.setSession(session);

          return teacher;
        }
      }

    } catch (SQLException e) {

      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return null;

  }

  public void delete(int id) {

    try {
      Connection connection = DbUtil.connectToDb();

      PreparedStatement stmt = connection.prepareStatement("DELETE FROM teacher WHERE id = ? ");
      stmt.setInt(1, id);

      stmt.executeUpdate();
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

  public boolean update(Teacher teacher) {

    try {
      Connection connection = DbUtil.connectToDb();

      PreparedStatement stmt = connection.prepareStatement(
          "UPDATE teacher SET name=?, first_name=?, birth_date=?, email=? WHERE id = ? ");

      stmt.setString(1, teacher.getName());
      stmt.setString(2, teacher.getFirstName());
      stmt.setDate(3, Date.valueOf(teacher.getBirthDate()));
      stmt.setString(4, teacher.getEmail());
      stmt.setInt(6, teacher.getId());

      stmt.executeUpdate();

      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return false;

  }



}
