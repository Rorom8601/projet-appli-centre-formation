package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import entity.Formation;

public class FormationRepository {
  public List<Formation> findAll() {
    List<Formation> list = new ArrayList<>();
    try (Connection connection = DbUtil.connectToDb()) {

      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM formation");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Formation formation = new Formation(rs.getInt("id"), rs.getString("label"),
            rs.getString("level"), rs.getInt("hours_duration"), rs.getString("description"));
        list.add(formation);
      }

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return list;
  }

  public Formation findById(int id) {
    try (Connection connection = DbUtil.connectToDb()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM formation WHERE id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Formation formation = new Formation(rs.getInt("id"), rs.getString("label"),
            rs.getString("level"), rs.getInt("hours_duration"), rs.getString("description"));
        System.out.println(rs.getString("label"));
        return formation;
      }
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return null;
  }

  public void save(Formation formation) {
    if (!(formation.getLabel() instanceof String)) {
      System.out.println("Certaines données d'entrée sont invalides");
    } else {
      try {
        Integer.parseInt(formation.getHoursDuration().toString());
        
      } catch (NumberFormatException e) {
        
      }

      try (Connection connection = DbUtil.connectToDb()) {
        PreparedStatement stmt = connection.prepareStatement(
            "INSERT INTO formation (label, level, hours_duration, description) VALUES (?,?,?,?)",
            PreparedStatement.RETURN_GENERATED_KEYS);

        stmt.setString(1, formation.getLabel());
        stmt.setString(2, formation.getLevel());
        stmt.setInt(3, formation.getHoursDuration());
        stmt.setString(4, formation.getDescription());
        stmt.executeUpdate();

        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
          formation.setId(rs.getInt(1));

        }

      } catch (SQLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

  }

  public void deleteById(int id) {
    try (Connection connection = DbUtil.connectToDb()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM formation WHERE id=?");

      stmt.setInt(1, id);
      stmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();

    }

  }

  public void update(Formation formation) {
    try (Connection connection = DbUtil.connectToDb()) {
      PreparedStatement stmt = connection.prepareStatement(
          "UPDATE formation SET label=?,level=?,hours_duration=?, description=? WHERE id=?");

      stmt.setString(1, formation.getLabel());
      stmt.setString(2, formation.getLevel());
      stmt.setInt(3, formation.getHoursDuration());
      stmt.setString(4, formation.getDescription());
      stmt.setInt(5, formation.getId());
      stmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();

    }
  }

}
