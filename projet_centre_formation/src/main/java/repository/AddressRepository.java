
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entity.Address;


public class AddressRepository {
  public List<Address> findAll() {
    List<Address> list = new ArrayList<>();
    try {
      Connection connection =DbUtil.connectToDb();
      PreparedStatement stmt = connection
          .prepareStatement("SELECT * FROM address");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Address address =
            new Address(rs.getInt("id"), rs.getString("number"), rs.getString("street"),
                rs.getString("zip_code"), rs.getString("city"), rs.getString("country"));
        list.add(address);
      }
      connection.close();

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  public void createAddress(Address address) {
    try {
      Connection connection =DbUtil.connectToDb();

      PreparedStatement stmt = connection
          .prepareStatement("INSERT INTO address( number, street, zip_code, city, country) VALUES (?,?,?,?,?)",
              PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1, address.getNumber());
      stmt.setString(2, address.getStreet());
      stmt.setString(3, address.getZipCode());
      stmt.setString(4, address.getCity());
      stmt.setString(5, address.getCountry());
      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
      address.setId(rs.getInt(1));
      }
      connection.close();

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }
  public void deleteAddressById(int id){
    try {
      Connection connection =DbUtil.connectToDb();

      PreparedStatement stmt=connection.prepareStatement("DELETE FROM address WHERE id= ?");
      stmt.setInt(1,id);
      stmt.executeUpdate();
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }


  public void updateAddress(Address address){
    try {
      Connection connection =DbUtil.connectToDb();
      PreparedStatement stmt=connection.prepareStatement("UPDATE address SET number= ?, street= ?, zip_code= ?, city=?, country=? WHERE id= ?");
      stmt.setString(1, address.getNumber());
      stmt.setString(2, address.getStreet());
      stmt.setString(3, address.getZipCode());
      stmt.setString(4, address.getCity());
      stmt.setString(5, address.getCountry());
      stmt.setInt(6, address.getId());
      stmt.executeUpdate();
      connection.close();

    } catch (SQLException e) {
     
      e.printStackTrace();
    }

  }

  public Address findAddressById(int id){
    try {
      Connection connection =DbUtil.connectToDb();
      PreparedStatement stmt=connection
      .prepareStatement("SELECT * FROM address where id=?");
      stmt.setInt(1,id);
      ResultSet rs = stmt.executeQuery();
      if(rs.next()){
        Address address=new Address(
          rs.getInt("id"),
          rs.getString("number"),
          rs.getString("street") ,
          rs.getString("zip_code") ,
          rs.getString("city") ,
          rs.getString("country") );
          connection.close();
          return address;
      }

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }

  public Address findAddressByIdTeacher(int id){
    try {
      Connection connection =DbUtil.connectToDb();
      PreparedStatement stmt=connection
      .prepareStatement("SELECT * FROM address INNER JOIN teacher ON address.id=teacher.id_address WHERE teacher.id=?");
      stmt.setInt(1,id);
      ResultSet rs = stmt.executeQuery();
      if(rs.next()){
        Address address=new Address(
          rs.getInt("id"),
          rs.getString("number"),
          rs.getString("street") ,
          rs.getString("zip_code") ,
          rs.getString("city") ,
          rs.getString("country") );
          connection.close();
          return address;
      }

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }

  public Address findAddressByIdStudent(int id){
    try {
      Connection connection =DbUtil.connectToDb();
      PreparedStatement stmt=connection
      .prepareStatement("SELECT * FROM address INNER JOIN student ON student.id_address=address.id WHERE student.id= ? ");
      stmt.setInt(1,id);
      ResultSet rs = stmt.executeQuery();
      if(rs.next()){
        Address address=new Address(
          rs.getInt("id"),
          rs.getString("number"),
          rs.getString("street") ,
          rs.getString("zip_code") ,
          rs.getString("city") ,
          rs.getString("country") );
          connection.close();
          return address;
      }

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }

  public Address findAddressByIdAdminStaff(int id){
    try {
      Connection connection =DbUtil.connectToDb();
      PreparedStatement stmt=connection
      .prepareStatement("SELECT * FROM address INNER JOIN admin_staff ON admin_staff.id_address=address.id WHERE admin_staff.id= ? ");
      stmt.setInt(1,id);
      ResultSet rs = stmt.executeQuery();
      if(rs.next()){
        Address address=new Address(
          rs.getInt("id"),
          rs.getString("number"),
          rs.getString("street") ,
          rs.getString("zip_code") ,
          rs.getString("city") ,
          rs.getString("country") );
          connection.close();
          return address;
      }

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }

}

