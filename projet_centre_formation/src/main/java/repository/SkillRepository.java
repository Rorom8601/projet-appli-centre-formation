package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import entity.Skill;

public class SkillRepository {
    public List<Skill> findAll() {
        List<Skill> list = new ArrayList<>();
        try {
            Connection connection = DbUtil.connectToDb();

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM skill");

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Skill sk = new Skill(rs.getInt("id"), rs.getString("label"),
                        rs.getString("description"));
                list.add(sk);
            }
            connection.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }

    public Skill findById(int id) {
        try {
            Connection connection = DbUtil.connectToDb();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM skill WHERE id =?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                Skill skill = new Skill(rs.getInt("id"), rs.getString("label"),
                        rs.getString("description"));
                return skill;

            }

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    public Skill create(Skill skill) {

        try {
            Connection connection = DbUtil.connectToDb();
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO skill(label,description) VALUES  ( ?, ?)",
                    PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, skill.getLabel());
            stmt.setString(2, skill.getDescription());

            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                skill.setId(rs.getInt(1));
            }

            connection.close();

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    public void delete(int id) {

        try {
            Connection connection = DbUtil.connectToDb();

            PreparedStatement stmt = connection.prepareStatement("DELETE FROM skill WHERE id = ? ");
            stmt.setInt(1, id);

            stmt.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public boolean update(Skill skill) {

        try {
            Connection connection = DbUtil.connectToDb();

            PreparedStatement stmt = connection
                    .prepareStatement("UPDATE skill SET label=?, description=?  WHERE id = ? ");


            stmt.setString(1, skill.getLabel());
            stmt.setString(2, skill.getDescription());
            stmt.setInt(3, skill.getId());

            stmt.executeUpdate();

            return stmt.getUpdateCount() == 1;

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;

    }

}
