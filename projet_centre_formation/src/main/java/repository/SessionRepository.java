package repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import entity.Address;
import entity.Formation;
import entity.Session;
import entity.Student;


public class SessionRepository {

  public List<Session> findAll() {
    List<Session> list = new ArrayList<>();

    try (Connection connection = DbUtil.connectToDb()) {

      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM session");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        int idSession = rs.getInt("id");
        Session session = new Session(idSession, rs.getDate("starting_date").toLocalDate(),
            rs.getDate("ending_date").toLocalDate());

        PreparedStatement stmtFormation = connection.prepareStatement(
            "SELECT * from formation INNER JOIN session ON session.id_formation=formation.id WHERE session.id= ? ");
        stmtFormation.setInt(1, idSession);
        ResultSet rsFormation = stmtFormation.executeQuery();

        if (rsFormation.next()) {
          Formation formation = new Formation(rsFormation.getInt("id"),
              rsFormation.getString("label"), rsFormation.getString("level"),
              rsFormation.getInt("hours_duration"), rsFormation.getString("description"));

          session.setFormation(formation);

          list.add(session);
        }
      }
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return list;
  }

  public Session findById(int id) {
    try (Connection connection = DbUtil.connectToDb()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM session WHERE id=?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Session session = new Session(rs.getInt("id"), rs.getDate("starting_date").toLocalDate(),
            rs.getDate("ending_date").toLocalDate());

        PreparedStatement stmtFormation = connection.prepareStatement(
            "SELECT * FROM formation INNER JOIN session ON session.id_formation=formation.id WHERE session.id= ? ");
        stmtFormation.setInt(1, id);
        ResultSet rsFormation = stmtFormation.executeQuery();

        if (rsFormation.next()) {
          Formation formation = new Formation(rsFormation.getInt("id"),
              rsFormation.getString("label"), rsFormation.getString("level"),
              rsFormation.getInt("hours_duration"), rsFormation.getString("description"));

          session.setFormation(formation);
        }

        return session;
      }
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return null;
  }

  public void deleteById(int id) {
    try (Connection connection = DbUtil.connectToDb()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM session WHERE id=?");

      stmt.setInt(1, id);


    } catch (SQLException e) {
      e.printStackTrace();

    }

  }


  public void update(Session session) {
    if ((session.getEndingDate().isBefore(session.getStartingDate()))) {
      System.out.println("Les dates ne sont pas cohérentes");

    } else {
      try (Connection connection = DbUtil.connectToDb()) {
        PreparedStatement stmt = connection.prepareStatement(
            "UPDATE session SET starting_date=?,ending_dated=?,id_formation=? WHERE id=?");
        // Trouver l id_formation
        Formation formation = session.getFormation();
        int idFormation = formation.getId();

        stmt.setDate(1, Date.valueOf(session.getStartingDate()));
        stmt.setDate(2, Date.valueOf(session.getEndingDate()));
        stmt.setInt(3, idFormation);
        stmt.setInt(4, session.getId());



      } catch (SQLException e) {
        e.printStackTrace();

      }
    }
  }


  public void save(Session session) {
    if ((session.getEndingDate().isBefore(session.getStartingDate()))) {
      System.out.println("Les dates ne sont pas cohérentes");

    } else {

      try (Connection connection = DbUtil.connectToDb()) {
        PreparedStatement stmt = connection.prepareStatement(
            "INSERT INTO session (starting_date, ending_date) VALUES (?,?)",
            PreparedStatement.RETURN_GENERATED_KEYS);

        stmt.setDate(1, Date.valueOf(session.getStartingDate()));
        stmt.setDate(2, Date.valueOf(session.getEndingDate()));
        stmt.executeUpdate();

        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
          session.setId(rs.getInt(1));
        }

      } catch (SQLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

  }

  public List<Student> getStudentListByIdSession(int id) {
    List<Student> list = new ArrayList<>();
    try (Connection connection = DbUtil.connectToDb()) {
      PreparedStatement stmt =
          connection.prepareStatement("SELECT * FROM student WHERE student.id_session= ? ");

      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        int idStudent = rs.getInt("id");
        String nom = rs.getString("name");
        String prenom = rs.getString("first_name");
        LocalDate date = rs.getDate("birth_date").toLocalDate();
        String addressEmail = rs.getString("mail_address");

        Student student = new Student();
        student.setId(idStudent);
        student.setName(nom);
        student.setFirstName(prenom);
        student.setBirthDate(date);
        student.setMailAddress(addressEmail);

        // Récuperer Session
        SessionRepository sr = new SessionRepository();
        Session session = new Session();
        session = sr.findById(id);
        student.setSession(session);

        // Recupérer address par studentID
        AddressRepository ar = new AddressRepository();
        Address address = new Address();
        address = ar.findAddressByIdStudent(idStudent);
        student.setAddress(address);

        list.add(student);
      }
    } catch (SQLException e) {
      e.printStackTrace();

    }
    return list;

  }

}
