package repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import entity.Address;
import entity.Session;
import entity.Student;

public class StudentRepository {
    public List<Student> findAll() {
        List<Student> list = new ArrayList<>();
        try {
            Connection connection = DbUtil.connectToDb();

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM student");

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                int idStudent = rs.getInt("id");
                Student student = new Student(idStudent, rs.getString("name"),
                        rs.getString("first_name"), rs.getDate("birth_date").toLocalDate(),
                        rs.getString("mail_address"));
                AddressRepository ad = new AddressRepository();
                Address address = new Address();
                address = ad.findAddressByIdStudent(idStudent);
                student.setAddress(address);
                list.add(student);

            }
            connection.close();

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return list;
    }


    public void save(Student student) {

        if (!(student.getName() instanceof String)
                || (!(student.getFirstName() instanceof String))) {
            System.out.println("Certaines données d'entrée ne sont pas du type attendu.");
        } else {



            try (Connection connection = DbUtil.connectToDb()) {
                PreparedStatement stmt = connection.prepareStatement(
                        "INSERT INTO student (name, first_name, birth_date, mail_address) VALUES (?,?,?,?)",
                        PreparedStatement.RETURN_GENERATED_KEYS);

                stmt.setString(1, student.getName());
                stmt.setString(2, student.getFirstName());
                stmt.setDate(3, Date.valueOf(student.getBirthDate()));
                stmt.setString(4, student.getMailAddress());

                stmt.executeUpdate();


                ResultSet rs = stmt.getGeneratedKeys();
                if (rs.next()) {

                    student.setId(rs.getInt(1));
                }

            } catch (SQLException e) {

                e.printStackTrace();
            }
        }

    }



    public Student findById(int id) {

        try (Connection connection = DbUtil.connectToDb()) {
            PreparedStatement stmt =
                    connection.prepareStatement("SELECT * FROM student WHERE id=?");

            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                int idStudent = rs.getInt("id");
                Student student = new Student(idStudent, rs.getString("name"),
                        rs.getString("first_name"), rs.getDate("birth_date").toLocalDate(),
                        rs.getString("mail_address"));


                AddressRepository ad = new AddressRepository();
                Address address = new Address();
                address = ad.findAddressById(idStudent);

                student.setAddress(address);

                // Recuperation de la session
                PreparedStatement stmtSession = connection.prepareStatement(
                        "SELECT * FROM session INNER JOIN student ON student .id_session=session.id WHERE student.id= ?");
                stmtSession.setInt(1, id);
                ResultSet rsSession = stmtSession.executeQuery();
                if (rsSession.next()) {
                    Session session = new Session(rsSession.getInt("id"),
                            rsSession.getDate("starting_date").toLocalDate(),
                            rsSession.getDate("ending_date").toLocalDate());
                    student.setSession(session);
                }
                return student;
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }

        return null;

    }



    public boolean deleteById(int id) {
        try (Connection connection = DbUtil.connectToDb()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM student WHERE id=?");

            stmt.setInt(1, id);

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return false;
    }



    public boolean update(Student student) {
        try (Connection connection = DbUtil.connectToDb()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE student SET name=?,first_name=?,birth_date=?,mail_address=? WHERE id=?");

            stmt.setString(1, student.getName());
            stmt.setString(2, student.getFirstName());
            stmt.setDate(3, Date.valueOf(student.getBirthDate()));
            stmt.setString(4, student.getMailAddress());
            stmt.setInt(5, student.getId());

            return stmt.executeUpdate() == 1;


        } catch (SQLException e) {
            e.printStackTrace();

        }
        return false;
    }

    public void attributeSkillToAStudent(int idSkill, int idStudent) {
        try {
            Connection connection = DbUtil.connectToDb();

            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO student_skill (id_skill, id_student) VALUES (?,?)");
            stmt.setInt(1, idSkill);
            stmt.setInt(2, idStudent);
            stmt.executeUpdate();
        } catch (SQLException e) {

            e.printStackTrace();
        }
    }

    public boolean checkIfStudentHasSpecificSkill(int idSkill, int idStudent) {
        try {
            Connection connection = DbUtil.connectToDb();

            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT * FROM student_skill WHERE id_skill= ? AND id_student= ? ");
            stmt.setInt(1, idSkill);
            stmt.setInt(2, idStudent);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return true;
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }


        return false;

    }

    // trouver le nombre de compétences validées par rappport au nombre à valider
    public String checkProgressionForCertification(int idStudent) {
        Double nbSkillsForFormation = 0.0;
        Double nbSkillPerStudent = 0.0;
        try {
            Connection connection = DbUtil.connectToDb();

            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT skill.id FROM skill INNER JOIN formation_skill ON formation_skill.id_skill=skill.id INNER JOIN formation ON formation.id=formation_skill.id_formation INNER JOIN session ON session.id_formation=formation.id INNER JOIN  student ON student.id_session=session.id WHERE student.id= ?");
            stmt.setInt(1, idStudent);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                // rs.getInt("id");
                nbSkillsForFormation++;
            }


            PreparedStatement stmt2 = connection
                    .prepareStatement("SELECT id_skill FROM student_skill where id_student= ?");
            stmt2.setInt(1, idStudent);
            ResultSet rs2 = stmt2.executeQuery();
            while (rs2.next()) {
                // rs.getInt("id");
                nbSkillPerStudent++;
            }
            final DecimalFormat df = new DecimalFormat("0.00");
            Double validationPourcentage = (nbSkillPerStudent / nbSkillsForFormation);
            // return validationPourcentage;
            String evolution = "Le pourcentage d'évolution de l'apprenant est de "
                    + df.format(validationPourcentage * 100) + " %";
            return evolution;

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;

    }



}
