package co.simplon.promo18;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import entity.Session;
import entity.Teacher;
import repository.TeacherRespository;
import entity.Address;
import entity.AdminStaff;
import entity.Formation;
import entity.Skill;
import repository.AddressRepository;
import repository.AdminStaffRepository;
import entity.Student;
import repository.AddressRepository;
import repository.FormationRepository;
import repository.SessionRepository;
import repository.SkillRepository;
import repository.StudentRepository;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) {

        //test FormationRepo
    FormationRepository fr=new FormationRepository();
    List<Formation> listFormation= new ArrayList<>();
    listFormation=fr.findAll();
    Formation formationId=fr.findById(3);
    Formation formationTest=new Formation("brancheur de cable HDMI", "Hyper dur", 3, "Vous verrez bien");
    Formation formationTest2=new Formation("débrancheur de cable HDMI", "Hyper dur", 5, "Vous verrez bien");
    //fr.save(formationTest);
    fr.save(formationTest2);
    //fr.deleteById(4);
    
    // Test Address
    // ad.createAddress(new Address("35", "rue du test", "75000", "Paris", "France"));
    // ad.deleteAddressById(14);
    AddressRepository ad = new AddressRepository();
    List<Address> list = new ArrayList<Address>();
    // Test FindAll
    list = ad.findAll();
    Address adrr=new Address();
    //test findAddressById()
    adrr=ad.findAddressById(1);
    for (Address address : list) {
      System.out.println(address.getStreet());
      
    }
    //test findAddressByIdTeacher()
    Address adrr2=new Address();
    adrr2=ad.findAddressByIdTeacher(2); 

    Address address = new Address(13, "12", "rue Belle", "75000", "Paris", "PaysTestUpdate");
    ad.updateAddress(address);
    Address address1 = ad.findAddressById(13);
    System.out.println(address.getCountry());
    Address address2 = ad.findAddressByIdTeacher(1);
    System.out.println(address);
    Address address3 = ad.findAddressByIdStudent(2);
    System.out.println(address);
    ad.findAddressByIdAdminStaff(1);

    //Tests StudentRepo
    ad.deleteAddressById(49);
    StudentRepository sr = new StudentRepository();
    //sr.attributeSkillToAStudent(2, 2);
    System.out.println(sr.checkProgressionForCertification(4));
    //sr.attributeSkillToAStudent(4, 1);
    System.out.println(sr.checkProgressionForCertification(6));

    //Tests SessionRepo
    SessionRepository sr3 = new SessionRepository();
    sr3.findById(1);
    sr3.findAll();
    fr.findById(1);
    List<Student> list2 = new ArrayList<Student>();
    list2 = sr3.getStudentListByIdSession(1);
    LocalDate date = LocalDate.of(2021, 1, 8);
    LocalDate date2 = LocalDate.of(2022, 10, 8);
    Session session = new Session(date2, date);//Mauvaise entrée (dates incohérentes)
    sr3.save(session);
    List <Student> stuList=new ArrayList();
    stuList=sr3.getStudentListByIdSession(1);

    //Test studentRepo
    StudentRepository stur = new StudentRepository();
    stur.findAll();
    Student student=new Student();
    student=stur.findById(1);
    LocalDate date3 = LocalDate.of(1984, 11, 10);
    Student student2=new Student("Paul", "Marie", date3, "mailAddress");
    stur.save(student2);

    //TestSkills
    SkillRepository skillRepository = new SkillRepository();
    List<Skill> listSkills=new ArrayList<>();
    listSkills=skillRepository.findAll();
    Skill skillId=skillRepository.findById(1);   
    Skill skillToAdd= new Skill("Exemple de skill", "description d un exemple de skill");
    //skillRepository.create(skillToAdd);  
    skillRepository.delete(6);
  

    //Test teacherRepo
    TeacherRespository tr =new TeacherRespository();
    List<Teacher> teacherList=new ArrayList<>();
    teacherList=tr.findAll();
    Teacher teacher=tr.findById(1);
    LocalDate dateTeacher = LocalDate.of(1980, 10, 8);
    Teacher te =new Teacher("Dupont", "Jacques",dateTeacher, "jaquot@gmail.com");
    //tr.createTeacher(te);
    tr.delete(17);

    //Test AdminStaff
    AdminStaffRepository as =new AdminStaffRepository();
    as.findAll();
    AdminStaff adminStaff=as.findById(1);
    LocalDate birthDate = LocalDate.of(1982, 10, 8);
    AdminStaff adminStaff2=new AdminStaff("Paul", "Durant", birthDate, "polo.dudu@wannadoo.fr");
    //as.save(adminStaff2);
    as.deleteById(4);
       
    }
}
