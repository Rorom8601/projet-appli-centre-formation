package entity;

import java.time.LocalDate;

public class Session {

  private Integer id;
  private LocalDate startingDate;
  private LocalDate endingDate;
  private Formation formation;

  

  public Session() {
    
  }

  public Session(Integer id, LocalDate startingDate, LocalDate endingDate) {
    this.id = id;
    this.startingDate = startingDate;
    this.endingDate = endingDate; 
  }

  public Session(LocalDate startingDate, LocalDate endingDate) {
    this.startingDate = startingDate;
    this.endingDate = endingDate;
  }

  public Session(Integer id, LocalDate startingDate, LocalDate endingDate, Formation formation) {
    this.id = id;
    this.startingDate = startingDate;
    this.endingDate = endingDate;
    this.formation = formation;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public LocalDate getStartingDate() {
    return startingDate;
  }

  public void setStartingDate(LocalDate startingDate) {
    this.startingDate = startingDate;
  }

  public LocalDate getEndingDate() {
    return endingDate;
  }

  public void setEndingDate(LocalDate endingDate) {
    this.endingDate = endingDate;
  }

  public Formation getFormation() {
    return formation;
  }

  public void setFormation(Formation formation) {
    this.formation = formation;
  }

}
