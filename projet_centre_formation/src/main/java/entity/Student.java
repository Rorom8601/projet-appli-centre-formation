package entity;

import java.sql.Date;
import java.time.LocalDate;

public class Student {
    

    private Integer id;
    private String name;
    private String firstName;
    private LocalDate birthDate;
    private String mailAddress;
    private Session Session;
    private Address Address;
    public Student() {
    }
    public Student(String name, String firstName, LocalDate birthDate, String mailAddress) {
        this.name = name;
        this.firstName = firstName;
        this.birthDate = birthDate;
        this.mailAddress = mailAddress;
        
    }
    public Student(Integer id, String name, String firstName, LocalDate birthDate, String mailAddress) {
        this.id = id;
        this.name = name;
        this.firstName = firstName;
        this.birthDate = birthDate;
        this.mailAddress = mailAddress;
        
    }
    public Student(int int1, String string, String string2, Date date) {
    }
    public Student(int int1, String string, String string2, LocalDate localDate, String string3) {
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public LocalDate getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }
    public String getMailAddress() {
        return mailAddress;
    }
    public void setMailAddress(String mailAddress) {
        this.mailAddress = mailAddress;
    }
    public Session getSession() {
        return Session;
    }
    public void setSession(Session session) {
        Session = session;
    }
    public Address getAddress() {
        return Address;
    }
    public void setAddress(Address address) {
        Address = address;
    }
    
}
   
