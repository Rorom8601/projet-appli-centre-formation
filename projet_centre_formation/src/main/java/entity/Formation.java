package entity;

public class Formation {
  private Integer id;
  private String label;
  private String level;
  private Integer hoursDuration;
  private String description;

  public Formation() {
    
  }

  public Formation(String label, String level, Integer hoursDuration, String description) {
    this.label = label;
    this.level = level;
    this.hoursDuration = hoursDuration;
    this.description = description;
  }
  public Formation(Integer id, String label, String level, Integer hoursDuration,
      String description) {
    this.id = id;
    this.label = label;
    this.level = level;
    this.hoursDuration = hoursDuration;
    this.description = description;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getLevel() {
    return level;
  }

  public void setLevel(String level) {
    this.level = level;
  }

  public Integer getHoursDuration() {
    return hoursDuration;
  }

  public void setHoursDuration(Integer hoursDuration) {
    this.hoursDuration = hoursDuration;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
 
  



}
