package entity;

import java.time.LocalDate;

public class AdminStaff {

  private Integer id;
  private String name;
  private String firstName;
  private LocalDate birthDate;
  private String mailAddress;
  private Address address;

  public AdminStaff(String name, String firstName, LocalDate birthDate, String mailAddress) {
    this.name = name;
    this.firstName = firstName;
    this.birthDate = birthDate;
    this.mailAddress = mailAddress;
  }

  public AdminStaff(Integer id, String name, String firstName, LocalDate birthDate,
      String mailAddress) {
    this.id = id;
    this.name = name;
    this.firstName = firstName;
    this.birthDate = birthDate;
    this.mailAddress = mailAddress;
  }

  public AdminStaff() {

  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public LocalDate getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(LocalDate birthDate) {
    this.birthDate = birthDate;
  }

  public String getMailAddress() {
    return mailAddress;
  }

  public void setMailAddress(String mailAddress) {
    this.mailAddress = mailAddress;
  }

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }



}
