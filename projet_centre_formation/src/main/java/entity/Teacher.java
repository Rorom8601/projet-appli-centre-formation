package entity;

import java.time.LocalDate;

public class Teacher {
  private Integer id ;
  private String name;
  private String firstName;
  private LocalDate birthDate;
  private String email;
  private Session session;
  private Address address;
  
  
  public Teacher() {
  }
  public Teacher(String name, String firstName, LocalDate birthDate, String email) {
    this.name = name;
    this.firstName = firstName;
    this.birthDate = birthDate;
    this.email = email;
  }
  public Teacher(Integer id, String name, String firstName, LocalDate date, String email) {
    this.id = id;
    this.name = name;
    this.firstName = firstName;
    this.birthDate = date;
    this.email = email;
  }
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public String getFirstName() {
    return firstName;
  }
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  public LocalDate getBirthDate() {
    return birthDate;
  }
  public void setBirthDate(LocalDate birthDate) {
    this.birthDate = birthDate;
  }
  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  public Session getSession() {
    return session;
  }
  public void setSession(Session session) {
    this.session = session;
  }
  public Address getAddress() {
    return address;
  }
  public void setAddress(Address address) {
    this.address = address;
  }
}
