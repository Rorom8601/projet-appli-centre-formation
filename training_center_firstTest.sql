--TESTS
--selectionner les étudiant suivant une formation dev web
select * from Student
inner join Session on Session.id=Student.id_session 
inner join Formation on Formation.id=Session.id_formation
where Formation.label like 'Dev%';

--selectionner les compétences requises pour la formation Dev web
select Skill.label from Skill

INNER join Formation_skill on Formation_skill.id_skill=Skill.id
inner join Formation on Formation_skill.id_formation=Formation.id
where Formation.label like 'Dev%';

--DONNER LADRESSE COMPLÈTE D UN ÉTUDIANT qui s appelle romain
SELECT * FROM `Address`
inner join `Student` on Address.id_student=Student.id
where Student.first_name like 'rom%';

--Chercher la liste des formateur pour la formation dev web
SELECT * FROM `Teacher`
INNER JOIN `Session`ON Session.id=Teacher.id_session
inner join `Formation` on Formation.id=Session.id_formation
WHERE Formation.label like 'Dev%';

--test avec la colonne test

--Valider la skill HTML pour Safik --PROBLEME AVEC JONCTION

SELECT * from Address
INNER JOIN Admin_staff ON Address.id_admin_staff=Admin_staff.id;

--Chercher la liste des adresses des élèves 
SELECT * FROM Address
INNER JOIN `Student` ON Student.id=Address.id_student;

--Chercher la liste des adresses des élèves de la formation Dev web
SELECT * FROM Address
INNER JOIN `Student` ON Student.id=Address.id_student
INNER JOIN `Session` ON Session.id=Student.id_session
INNER JOIN `Formation` ON Formation.id=Session.id_formation
Where Formation.label like 'Dev%';

use training_center;
select * from Student_skill;

select * from address;

-- obtenir l addresse d un teacher
SELECT * FROM address
INNER JOIN teacher ON address.id=teacher.id_address
where teacher.id=1;

SELECT * FROM address
INNER JOIN student ON student.id_address=address.id
where student.id=1;

--les etudiants d une session
SELECT * FROM student
WHERE id_session= 1;

SELECT * FROM student_skill
WHERE id_skill=1 AND id_student=1;
;

INSERT INTO student_skill (id_skill, id_student VALUES (2,3);
