DROP DATABASE IF EXISTS training_center;
CREATE DATABASE IF NOT EXISTS training_center;
USE training_center;

CREATE TABLE address (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  number VARCHAR(64) NOT NULL,
  street VARCHAR(600) NOT NULL,
  zip_code VARCHAR(15) NOT NULL,
  city VARCHAR (150) NOT NULL, 
  country VARCHAR (150) NOT NULL
);
CREATE TABLE skill (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  label VARCHAR(150) NOT NULL,
  description VARCHAR(1000) NOT NULL
);
CREATE TABLE admin_staff (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(64) ,
  first_name VARCHAR(64) NOT NULL,
  birth_date DATE NOT NULL,
  mail_address VARCHAR (150) NOT NULL,
  id_address INT UNSIGNED,
  CONSTRAINT FK_adminstaff_address FOREIGN KEY (id_address) REFERENCES address(id)
);

CREATE TABLE formation (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  label VARCHAR (150) NOT NULL,
  level VARCHAR (64) NOT NULL,
  hours_duration INT NOT NULL,
  description VARCHAR(1000) NOT NULL
);

CREATE TABLE session (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  starting_date DATE NOT NULL,
  ending_date DATE NOT NULL,
  id_formation INT UNSIGNED,
  CONSTRAINT FK_session_formation FOREIGN KEY (id_formation) REFERENCES formation(id)
);
CREATE TABLE student (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(64) NOT NULL,
  first_name VARCHAR(64) NOT NULL,
  birth_date DATE NOT NULL,
  mail_address VARCHAR (150) NOT NULL,
  id_session INT UNSIGNED,
  id_address INT UNSIGNED,
  CONSTRAINT FK_student_session FOREIGN KEY (id_session) REFERENCES session(id),
  CONSTRAINT FK_student_adress FOREIGN KEY (id_address) REFERENCES address(id)
);

CREATE TABLE teacher (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(64) NOT NULL,
  first_name VARCHAR(64)NOT NULL,
  birth_date DATE NOT NULL,
  mail_address VARCHAR (150) NOT NULL,
  id_session INT UNSIGNED,
  id_address INT UNSIGNED,
  CONSTRAINT FK_teacher_session FOREIGN KEY (id_session) REFERENCES session(id),
  CONSTRAINT FK_teacher_adress FOREIGN KEY (id_address) REFERENCES address(id)
);

CREATE TABLE formation_skill (
  id_formation INT UNSIGNED,
  id_skill INT UNSIGNED,
  PRIMARY KEY (id_formation,id_skill),
  CONSTRAINT FK_formationskill_skill FOREIGN KEY (id_skill) REFERENCES skill(id),
  CONSTRAINT FK_formationskill_formation FOREIGN KEY (id_formation) REFERENCES formation(id)
);

CREATE TABLE student_skill (
  id_skill INT UNSIGNED,
  id_student INT UNSIGNED,
  PRIMARY KEY (id_skill,id_student),
  CONSTRAINT FK_student_skill FOREIGN KEY (id_skill) REFERENCES skill(id),
  CONSTRAINT FK_skill_student FOREIGN KEY (id_student) REFERENCES student(id)
);

INSERT into address(number, street, city, zip_code, country) VALUES
('2 bis', 'rue du stade', 'Villeurbanne','69100', 'France'),
('3', 'rue de la foret', 'Villefranche','69400', 'France'),
('1', 'Grande rue', 'Chazey sur Ain','01150', 'France'),
('7', 'rue du stade', 'Lyon','69003','France'),
('2 bis', 'rue du stade', 'Paris','75000', 'France'),
('2 bis', 'rue de lese maryam', 'Kaboul','69100', 'Afghanistan'),
('7', 'quai de Saône', 'Lyon','69003', 'France'),
('67', 'rue Française', 'Villeurbanne','69100', 'France'),
('72', 'rue Maurice Moissonier', 'Vaux en Velin','69120', 'France'),
('1', 'rue Emile Zola', 'Lyon','69003', 'France'),
('17', 'rue de la Liberté', 'Lyon','69002', 'France'),
('56', 'rue Gambetta','Lyon','69003', 'France'),
('159', 'rue maréchal Foch', 'Lyon','69007', 'France');

INSERT INTO skill(label, description) VALUES
('JAVA', 'langage objet'),
('C++', 'langage objet'),
('HTML', 'langage déclaratif'),
('Ruby', 'logiciel de d''exploitation de données'),
('GIT', 'logiciel de versionning');

INSERT INTO admin_staff(name,first_name,birth_date, mail_address,id_address) VALUES
('Garcia','Annaelle','1994-02-01', 'anna@simplon.com',5),
('Ducreux','Charlene','1978-02-01', 'char@simplon.com',6),
('Coronilla','Stephen','1995-02-01', 'steph@simplon.com',7);

INSERT into formation(label, level, hours_duration, description)
VALUES ('Dev. web mobile', 'Certif niveau 5-Bac+2',800, 'forme au métier de dev mobile sur JAVA'),
('Concepteur développeur d''applications', "Certif niveau 6-Bac+4",950, "forme au métier de developpeur sur C++"),
('Data Scientists', 'Certif niveau 5-Bac+2',800, 'forme au métiers liés au traitement des données');

INSERT INTO session (starting_date, ending_date, id_formation) VALUES
('2022-03-14','2022-09-15',1),
('2022-10-15','2023-05-15',1),
('2022-10-15','2023-07-15',2),
('2022-03-14','2022-09-15',3);

INSERT INTO student (name,first_name,birth_date,mail_address, id_session, id_address) VALUES
('Qus','Safik','1980-02-09','safik123@gmail.com',1,8),
('SHIRZAI','Mohammad','1998-05-18','mohammad23@gmail.com',1,9),
('SIRE','Romain','1984-10-11','rom23@gmail.com',2,10),
('TASSO','Camille ','1988-07-18','camille123@gmail.com',2,11),
('Bertrant','Jacky','1989-02-09','jacky123@gmail.com',2,12),
('Doe','John','1990-02-09','john123@gmail.com',3,13);

INSERT INTO teacher(name,first_name,birth_date, mail_address, id_session,id_address) VALUES
('Demel','Jean', '1985-01-01','jannot@simplon.com',1,1),
('Delue','Barth', '1986-01-01','bart@simplon.com',1,2),
('Dever','John', '1985-01-01','john@simplon.com',3,3),
('Durant','Samy', '1979-01-01','sam@simplon.com',2,4);

INSERT INTO formation_skill(id_formation,id_skill) VALUES
(1,1),
(1,3),
(1,5),
(2,2),
(2,5),
(3,4),
(3,5);


INSERT INTO student_skill(id_skill, id_student) VALUES
(1,1),
(1,2),
(1,3),
(1,4),
(4,5),
(2,6);